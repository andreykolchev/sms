package sms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sms.integration.client.RestClient;
import sms.model.Order;
import sms.repository.OrderRepository;

/**
 * Created by user on 18.02.16.
 */
@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private RestClient restClient;

    @RequestMapping(value = "/addOrder", method = RequestMethod.POST)
    public ResponseEntity postOrder(@RequestBody Order order) {
        try {
            Order new_order = orderRepository.save(order);//create in database
            if (new_order!=null){
                restClient.postOrder(new_order);//post to 1c
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
