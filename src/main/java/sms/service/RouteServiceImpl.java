package sms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import sms.model.Agent;
import sms.model.Route;
import sms.model.dto.RouteDto;
import sms.repository.AgentRepository;
import sms.repository.RouteRepository;
import sms.util.DtoFactory;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by user on 22.02.16.
 */
@Service
public class RouteServiceImpl implements RouteService {

    @Autowired
    private RouteRepository routeRepository;
    @Autowired
    private AgentRepository agentRepository;

    @Override
    public List<RouteDto> getForAgent() {
        List<Route> routs = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Agent agent = agentRepository.findByUsername(auth.getName());
        if (Objects.nonNull(agent)) {
            try {
                routs = routeRepository.findByDateAndAgent(new Date(), agent);
            }catch (Exception ex){}
        }
        return  DtoFactory.createListOfRoutsDto(routs);
    }



}
