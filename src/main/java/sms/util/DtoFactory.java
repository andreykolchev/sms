package sms.util;

import sms.model.Route;
import sms.model.TradePoint;
import sms.model.dto.RouteDto;
import sms.model.dto.TradePointDto;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by user on 22.02.16.
 */
public class DtoFactory {


    public static TradePointDto createTradePointDto(TradePoint entity) {
        TradePointDto dto = new TradePointDto();
        dto.setId(entity.getId());
        dto.setDescription(entity.getDescription());
        dto.setContact(entity.getContact());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setRegion(entity.getRegion());
        dto.setAddress(entity.getAddress());
        dto.setLatitude(entity.getLatitude());
        dto.setLongitude(entity.getLongitude());
        return dto;
    }

    public static RouteDto createRouteDto(Route entity) {
        RouteDto dto = new RouteDto();
        dto.setId(entity.getId());
        if (Objects.nonNull(entity.getDate())) {
            dto.setDate(new Date(entity.getDate().getTime()));
        }
        dto.setPriority(entity.getPriority());
        dto.setDateTime(entity.getDateTime());
        if (Objects.nonNull(entity.getTradePoint())) {
            dto.setTradePoint(createTradePointDto(entity.getTradePoint()));
        }
        return dto;
    }

    public static List<RouteDto> createListOfRoutsDto(Collection<Route> routs){
        return routs.stream().map(route -> createRouteDto(route)).collect(Collectors.toList());
    }



}
