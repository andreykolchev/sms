package sms.model;

import lombok.Data;
import javax.persistence.*;

/**
 * Created by Andrey on 20.02.2016.
 */
@Data
@Entity
@Table(name = "order_row")
public class OrderRow {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "order_id")
    private Order order;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "item_id")
    private Item item;

    private double price;

    private double quantity;

    private double sum;

}
