package sms.model;

import lombok.Data;
import javax.persistence.*;

/**
 * Created by Andrey on 20.02.2016.
 */

@Data
@Entity
@Table(name = "route", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private java.sql.Date date;

    private int priority;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "agent_id")
    Agent agent;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "trade_point_id")
    TradePoint tradePoint;

    @Column(name = "date_time")
    private java.util.Date dateTime;

}

