package sms.model;

import lombok.Data;
import javax.persistence.*;

/**
 * Created by Andrey on 20.02.2016.
 */
@Data
@Entity
@Table(name = "firm", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id"),
        @UniqueConstraint(columnNames = "integration_id")
})
public class Firm {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "integration_id")
    private String integrationId;

    private String description;

}
