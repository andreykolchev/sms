package sms.model;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by user on 18.02.16.
 */
@Data
@Entity
@Table(name = "order", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name="integration_id")
    private String integrationId;

    @NotNull
    @Column(name="date_time")
    private java.util.Date dateTime;

    private String description;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "firm_id")
    private Firm firm;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name="route_id")
    private Route route;

    @OneToMany(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "order_row_id")
    private Set<OrderRow> rows;

    private double sum;

    @Column(name = "shipping_date")
    private java.sql.Date shippingDate;

    @Column(name = "payment_date")
    private java.sql.Date paymentDate;

}
