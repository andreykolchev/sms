package sms.model;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Andrey on 20.02.2016.
 */
@Data
@Entity
@Table(name = "presenter", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")
})
public class Presenter {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "items_id")
    private Item item;

    @NotNull
    private int priority;

    @Column(columnDefinition = "LONGBLOB")
    private String image;

}
