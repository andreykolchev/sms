package sms.model;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Andrey on 20.02.2016.
 */
@Data
@Entity
@Table(name = "agent", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id"),
        @UniqueConstraint(columnNames = "integration_id")
})
public class Agent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="integration_id")
    private String integrationId;

    @NotNull
    private String username;

    private String email;

    @NotNull
    private String password;

    @Column(name = "create_time")
    private Date createTime;

}
