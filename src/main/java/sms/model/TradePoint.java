package sms.model;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Andrey on 20.02.2016.
 */
@Data
@Entity
@Table(name = "trade_point", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id"),
        @UniqueConstraint(columnNames = "integration_id")
})
public class TradePoint {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "integration_id")
    private String integrationId;

    @NotNull
    private String description;

    private String contact;

    @NotNull
    @Column(name = "phone_number")
    private String phoneNumber;

    private String region;

    private String address;

    private Double latitude;

    private Double longitude;

}
