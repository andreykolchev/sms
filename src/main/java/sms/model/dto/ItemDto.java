package sms.model.dto;

import lombok.Data;

/**
 * Created by user on 22.02.16.
 */
@Data
public class ItemDto {

    private Long id;
    private String integrationId;
    private String description;
    private Long categoryId;

}
