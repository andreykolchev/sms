package sms.model.dto;

import lombok.Data;
import java.util.List;

/**
 * Created by user on 22.02.16.
 */
@Data
public class CategoryDto {

    private Long id;
    private String name;
    private String integrationId;
    private Long parentId;
    private List<ItemDto> items;

}
